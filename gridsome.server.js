// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const people = require('./src/assets/api/people.json')

module.exports = function(api) {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/

    api.loadSource(store => {
        const contentType = store.addCollection({
            typeName: 'People'
        })

        for (const item of people) {
            contentType.addNode({
                id: item.id,
                name: item.name,
                title: item.title,
                about: item.about,
                image: item.image
            })
        }
    })

    api.createPages(({ createPage }) => {
        // Use the Pages API here: https://gridsome.org/docs/pages-api/
    })
}
