module.exports = {
    theme: {
        container: {
            center: true,
            padding: '1.6rem'
        },
        fontSize: {
            xs: '1rem',
            sm: '1.2rem',
            tiny: '1.4rem',
            base: '1.6rem',
            lg: '1.8rem',
            xl: '2rem',
            h1: '4.2rem',
            h2: '3.2rem',
            h3: '2.6rem'
        },
        extend: {
            height: {
                350: '35rem',
                300: '30rem',
                250: '25rem'
            },
            colors: {
                theme: {
                    primary: 'var(--bg-theme-primary)',
                    secondary: 'var(--bg-theme-secondary)',

                    form: 'var(--bg-theme-form)'
                },

                copy: {
                    primary: 'var(--text-copy-primary)',
                    secondary: 'var(--text-copy-secondary)'
                },

                'border-color': {
                    primary: 'var(--border-border-color-primary)',
                    secondary: 'var(--border-border-color-secondary)',
                    ternary: 'var(--border-border-color-ternary)'
                },
                astecha: {
                    primary: '#10293b',
                    secondary: '#ec1a3a',
                    'secondary-hover': '#f62444',
                    'secondary-active': '#e21030',
                    gray: '#f5f5f7',
                    dedede: '#dedede',
                    ternary: '#302c50'
                }
            }
        }
    },
    variants: {
        fontSize: ['responsive', 'hover', 'focus'],
        backgroundColor: ['responsive', 'hover', 'focus', 'active']
    },
    plugins: []
}
